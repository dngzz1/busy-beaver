import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.TreeMap;

public class BusyBeaver {
	public static final boolean PRINT_OUT_EVERY_STEP = true;
	public static final boolean VERBOSE_PRINTING = true;
	public static final boolean PRINT_AT_END_OF_PROGRAMME_TO_CONSOLE = true;

	public static void main(String[] args) throws FileNotFoundException {
		/*
		 * Parameters for programme.
		 */

		/*
		 * Initialization.
		 */
		// Prepare text file for printing.
		PrintWriter out = new PrintWriter("beaverLog.txt");

		// Initialize river.
		Map<Integer, Integer> river = new TreeMap<>();
		river.put(0, 0);
		for(int j = -15; j <= 3; j++) {
			river.put(j, 0);
		}
		// Initialize beaver.
		Beaver beaver = new Beaver();

		// Set up timing.
		long startTime = System.currentTimeMillis();

		/*
		 * Main programme.
		 */
		for (int i = 1; i < 50000000; i++) {
			beaver.takeAction(river);
			BusyBeaver.printRiver(out, river, beaver);

			// halt if busy beaver is H.
			if (beaver.getState().equals("*")) {
				out.println("Beaver halts at step " + i + ".");
				break;
			}

		}

		/*
		 * Finish off the calculations.
		 */
		long endTime = System.currentTimeMillis();
		out.println("Time taken: " + (endTime - startTime) + " ms.");

		// calculate number of 1's printed.
		int score = 0;
		for (Integer key : river.keySet()) {
			score += river.get(key);
		}
		out.println("Number of 1's: " + score);

		// calculate minimum and maximum position.
		out.println("Min position: " + beaver.getPositionMin());
		out.println("Max position: " + beaver.getPositionMax());

		// close PrintWriter.
		out.close();

		/*
		 * Display text file to system.
		 */
		// The name of file to open.
		if (PRINT_AT_END_OF_PROGRAMME_TO_CONSOLE) {
			String fileName = "beaverLog.txt";
			// This will reference one line at a time.
			String line = null;

			try {
				// FileReader reads text files in the default encoding.
				FileReader fileReader = new FileReader(fileName);

				// Always wrap FileReader in BufferedReader.
				BufferedReader bufferedReader = new BufferedReader(fileReader);

				while ((line = bufferedReader.readLine()) != null) {
					System.out.println(line);
				}

				// Always close files.
				bufferedReader.close();
			} catch (FileNotFoundException ex) {
				System.out.println("Unable to open file");
			} catch (IOException ex) {
				System.out.println("Error reading file");
			}
		}
		System.out.println("Programme finished.");
	}

	public static void printRiver(PrintWriter out, Map<Integer, Integer> river, Beaver beaver) {
		if (!PRINT_OUT_EVERY_STEP) {
			return;
		}
		
		if (BusyBeaver.VERBOSE_PRINTING) {
			/*
			 * Verbose printing.
			 */
			int beaverPosition = beaver.getPosition();
			String beaverState = beaver.getState();
			for (Integer key : river.keySet()) {
				if (beaver.getPosition() == key) {
					out.printf(beaver.getState() + "");
				} else {
					out.printf(" ");
				}
				out.printf(river.get(key) + "  ");
			}
			out.println();
		} else {

			/*
			 * Shortened printing.
			 */
			int memoryCount = 0;
			String memoryValue = "X";
			int beaverPosition = beaver.getPosition();
			String beaverState = beaver.getState();

			// find out current marker to print.
			for (Integer key : river.keySet()) {
				String beaverMarker = (beaverPosition == key) ? beaverState : "";
				String currentMarker = beaverMarker + Integer.toString(river.get(key));

				// debug
//			System.out.println("\nPosition is: " + key);
//			System.out.println("Current marker is: " + currentMarker);
//			System.out.println("Memory count is: " + memoryCount);
//			System.out.println("Memory value is: " + memoryValue);

				// for the first iteration, set currentValue.
				if (memoryValue.equals("X")) {
					memoryValue = currentMarker;
					memoryCount = 1;
					continue;
				}
				// collect long runs for 0's and 1's.
				if (currentMarker.equals(memoryValue)) {
					memoryCount++;
					continue;
				} else {
					printFromMemory(out, memoryCount, memoryValue);
					memoryValue = currentMarker;
					memoryCount = 1;
					continue;
				}
			}
			printFromMemory(out, memoryCount, memoryValue);
			out.println("");
		}
	}

	public static void printFromMemory(PrintWriter out, int memoryCount, String memoryValue) {
		if (memoryCount >= 3) {
			out.printf("%2s...[%3d]...%s   ", memoryValue, memoryCount, memoryValue);
		} else if (memoryCount == 2) {
			out.printf("%2s   %2s   ", memoryValue, memoryValue);
		} else if (memoryCount == 1) {
			out.printf("%2s   ", memoryValue);
		}
	}

}
