import java.util.Map;

public class Beaver {

	/*
	 * Current Data.
	 */
	// usually state is denoted A,B,C,D...
	// here we will use 0,1,2,3,...
	// but for display purposes we will convert to alphabets.
	private int state = 0;
	public final static String STATE_CHARS = "ABCDEFGHIJKLMNOPQR*";
	private int position = 0;

	/*
	 * Collecting statistics.
	 */
	private int positionMin = 0;
	private int positionMax = 0;

	/*
	 * Algorithm for moving. The table is read: A B C D ... 0 *** *** *** *** 1 ***
	 * *** *** ***
	 */
	
	// 4 state, 2 symbol busy
	public String ruleTable[][] = {
			{"1RB","1LA","1R*","1RD"},
			{"1LB","0LC","1LD","0RA"}
	};
	
	// 5 state, 2 symbol best
//	public String ruleTable[][] = { 
//			{ "1RB", "1RC", "1RD", "1LA","1R*" }, 
//			{ "1LC", "1RB", "0LE", "1LD","0LA" } 
//			};

	/*
	 * Given a river, the beaver takes action.
	 */
	public void takeAction(Map<Integer, Integer> river) {
		// Beaver makes a reading at current position.
		int reading;
		if (river.containsKey(position)) {
			reading = river.get(position);
		} else {
			reading = 0;
		}
		
		// Beaver looks up action according to rule book.
		String action = ruleTable[reading][state];
		// Now take action. First do rewriting.
		river.put(position, Integer.parseInt(action.substring(0, 1)));

		// now move left or right.
		switch (action.substring(1, 2)) {
		case "L":
			position--;
			if (position < positionMin) {
				positionMin = position;
			}
			break;
		case "R":
			position++;
			if (position > positionMax) {
				positionMax = position;
			}
			break;
		}

		// now change river state at current position.
		state = STATE_CHARS.indexOf(action.charAt(2));
	}

	/*
	 * Getters and Setters.
	 */
	public int getPosition() {
		return position;
	}

	public String getState() {
		return Character.toString(STATE_CHARS.charAt(state));
	}

	public int getPositionMin() {
		return positionMin;
	}

	public int getPositionMax() {
		return positionMax;
	}

}
